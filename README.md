# ALARMA DE TEMPERATURA

## Problema

Se requiere un sistema de control en una fábrica que esté monitoreando continuamente la temperatura de un tanque, esta temperatura debe oscilar normalmente entre 0 y 50 grados centígrados, si la temperatura sobrepasa este valor en un rango de 50 a 60 grados se dispara una alarma de advertencia, si la temperatura sobrepasa los 60 grados el personal está en peligro, debe activar otra alarma visual y auditiva y abrir una puerta activando un servomotor.

## ¿Qué hace?

El sensor TMP36 lee la temperatura y el arduino toma el dato:
- Si es menor a 50°C se mantendrá encendido el LED verde, se mostrará la palabra "OK" en la pantalla y el servo motor girará para mantener cerrada la puerta.
- Si está entre 50°C y 60°C el LED se pondrá rojo, se activará la alarma sonora y se encenderá la retroiluminación de la pantalla, además de mostrar la palabra "PRECAUCION" en ella.
- Si es igual o mayor de 60°C, el LED permanecerá rojo, la alarma seguirá sonando y la pantalla se mantendrá encendida mostrando ahora la palabra "PELIGRO", pero adicionalmente el servo motor girará para abrir la puerta de salida.

## Simulación:

El proyecto está disponible en https://www.tinkercad.com/things/1lsWVhcdjIE

### Componentes:
| Nombre | Cantidad | Componente |
| --- | --- | --- |
| UArduino_01 | 1 | Arduino Uno |
| UTMP_01 | 1 | Sensor de temperatura [TMP36] |
| PIEZOSpeaker_01 | 1 | Alarma sonora |
| D1 | 1 | LED RGB |
| R1, R2, R3 | 3 | 220 Ω Resistencia |
| SERVO2 | 1 | Posicional Microservomotor |
| U2 | 1 | Display basado en MCP23008, 32 LCD 16 x 2 (I2C) |

### Código para el Arduino:

```cpp
// C++ code
// Javier Leonardo Cerón Puentes
/*
  Se requiere un sistema de control en una fábrica que esté 
  monitoreando continuamente la temperatura de un tanque, 
  esta temperatura debe oscilar normalmente entre 0 y 50 grados 
  centígrados, si la temperatura sobrepasa este valor en un rango 
  de 50 a 60 grados se dispara una alarma de advertencia, si la 
  temperatura sobrepasa los 60 grados el personal está en peligro, 
  debe activar otra alarma visual y auditiva y abrir una puerta 
  activando un servomotor

  El cálculo de los grados celsius es tomado de:
  https://cursos.mcielectronics.cl/2022/08/01/como-utilizar-el-sensor-de-temperatura-tmp36-tutorial-de-arduino/
*/

#include <Servo.h>
#include <Adafruit_LiquidCrystal.h>

Servo servoMotor;
Adafruit_LiquidCrystal lcd_1(0);

int celsius = 0;
int previo = -1;
int pos = 0;
const int tempCuidado = 50;
const int tempCritica = 60;

const int ledRojo = 11;
const int ledAzul = 10;
const int ledVerde = 9;

const int pinSpeaker = 8;
const int pinServo = 7;

void setup()
{
  pinMode(A0, INPUT);
  pinMode(pinSpeaker, OUTPUT);
  pinMode(ledVerde, OUTPUT);
  pinMode(ledRojo, OUTPUT);
  servoMotor.attach(pinServo, 500, 2500);
  lcd_1.begin(16, 2);
  Serial.begin(9600);
}

void loop()
{
  previo = celsius;
  celsius = ((analogRead(A0) * 0.004882814) - 0.5) * 100.0;
  abrirPuerta(celsius);

  if (previo != celsius)
  {
    mostrar(celsius);

    if (celsius > tempCuidado)
    {
      alarmaSonora();
      alarmaVisual();
    }
    else
    {
      ok();
    }
  }
}

void mostrar(int temp)
{
  String mensaje = "OK     ";
  lcd_1.setBacklight(0);
  if (celsius >= tempCritica)
  {
  	lcd_1.setBacklight(1);
    mensaje = "PELIGRO";
  }
  else {
    if(celsius >= tempCuidado)
    {
      mensaje = "CUIDADO";
    }
  }

  lcd_1.setCursor(0, 1);
  lcd_1.print(temp);
  lcd_1.setBacklight(celsius > tempCuidado);
  lcd_1.setCursor(0, 0);
  lcd_1.print(mensaje);
}

void warning()
{
  digitalWrite(ledVerde, HIGH);
  digitalWrite(ledRojo, HIGH);
}

void abrirPuerta(int temp)
{
  if(temp >= tempCritica)
  {
    if(pos < 180)
    {
      pos++;
    }
  }
  else
  {
    if(pos > 0)
    {
      pos--;
    }
  }
  servoMotor.write(pos);
}

void alarmaSonora()
{
  digitalWrite(pinSpeaker, HIGH);
  digitalWrite(ledRojo, LOW);
}

void ok()
{
  digitalWrite(ledVerde, HIGH);
  digitalWrite(ledRojo, LOW);
  digitalWrite(pinSpeaker, LOW);
}

void alarmaVisual()
{
  digitalWrite(ledRojo, HIGH);
  digitalWrite(ledVerde, LOW);
}
```
